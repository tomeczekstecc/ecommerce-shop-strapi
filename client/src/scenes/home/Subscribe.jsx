import {Box, IconButton, Typography} from "@mui/material";
import {MarkEmailReadOutlined} from "@mui/icons-material";
import {useState} from "react";


const Subscribe = () => {
    const [email, setEmail] = useState('');

    return (
        <Box
            width={'80%'}
            margin={'80px auto'}
            textAlign={'center'}
        >
            <IconButton>
                <MarkEmailReadOutlined fontSize={'large'}/>
            </IconButton>
            <Typography variant={'h3'}>
                Subscribe to our newsletter
            </Typography>
            <Typography variant={'h6'} component={'span'}>Get 10% off your first
                order
            </Typography>
            <Box
                padding='2px 4px'
                display='flex'
                m={'15px auto'}
                width={'75%'}
                backgroundColor={'#f5f5f5'}
                alignItems={'center'}
            >

            </Box>
        </Box>
    )
}

export default Subscribe

// import {useDispatch, useSelector } from "react-redux";
import {Badge, Box, IconButton} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {MenuOutlined, PersonOutline, SearchOutlined, ShoppingBagOutlined} from "@mui/icons-material";
import {setIsCartOpen} from "../../state";

const Navbar = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart.cart);
    const isCartOpen = useSelector(state => state.cart.isCartOpen);


    return (
        <div>
            <Box sx={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                height: '64px',
                // backgroundColor: "rgba(0,0,0,0.95)",
                // color: shades.neutral[100],
                position: "fixed",
                top: "0",
                left: "0",
                zIndex: "1",
            }}>
                <Box sx={{
                    display: 'flex',
                    width: '80%',
                    margin: 'auto',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                    <Box onClick={() => navigate('/')}
                         sx={{
                             '&:hover': {
                                 cursor: 'pointer',
                                 // color: shades.secondary[500],
                             }
                         }}
                    >
                        ECOMMER
                    </Box>
                    <Box
                        display={'flex'}
                        justifyContent={'space-between'}
                        columnGap={'20px'}
                        zIndex={'2'}

                    >
                        <IconButton sx={{color: 'black'}}>
                            <SearchOutlined/>
                        </IconButton>

                        <IconButton sx={{color: 'black'}}>
                            <PersonOutline/>
                        </IconButton>

                        <Badge
                            // showZero
                            invisible={cart.length === 0}
                            badgeContent={cart.length || 0} color="secondary" sx={{
                            "& .MuiBadge-badge": {
                                right: 5,
                                top: 5,
                                padding: '0 4px',
                                paddingTop: '2px',
                                height: '14px',
                                minWidth: '13px',
                            }
                        }}>

                            <IconButton sx={{color: 'black'}}
                                        onClick={() => dispatch(setIsCartOpen())}><ShoppingBagOutlined/>
                            </IconButton>
                        </Badge>
                        <IconButton sx={{color: 'black'}}><MenuOutlined/>
                        </IconButton>
                    </Box>
                </Box>
            </Box>
        </div>
    )
}

export default Navbar

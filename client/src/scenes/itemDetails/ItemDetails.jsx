import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {IconButton, Button, Box, Typography, Tabs, Tab} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import RemoveIcon from "@mui/icons-material/Remove";
import {shades} from "../../theme";
import {addTocart, decreaseCount, increaseCount} from "../../state";
import {useParams} from "react-router-dom";
import Item from "../../components/Item";

const ItemDetails = () => {
    const dispatch = useDispatch();
    const {itemId} = useParams();
    const [value, setValue] = useState('description');
    const [count, setCount] = useState(1);
    const [item, setItem] = useState(null);
    const [items, setItems] = useState([]);
    const handleChange = (event, newValue) => {
        setValue(newValue);

    }

    async function fetchItem() {
        const response = await fetch(`http://localhost:1337/api/items/${itemId}?populate=image`);
        const data = await response.json();
        setItem(data);
    }

    async function fetchItems() {
        const response = await fetch(`http://localhost:1337/api/items?populate=image`);
        const data = await response.json();
        setItems(data);
    }

    useEffect(
        () => {
            fetchItem()
            fetchItems()
            return () => {
                setItem(null);
                setItems([]);
            }
        }, [itemId]
    )

    console.log(item)

    return (
        <Box
            width={'80%'}
            m={'80px auto'}
        >
            <Box
                display={'flex'}
                flexWrap={'wrap'}
                columnGap={'40px'}
            >
                {/*IMAGES*/}
                <Box
                    flex={'1 1 40%'}
                    mb={'40px'}
                >
                    <img alt={item?.name}
                         width={'100%'}
                         height={'100%'}
                         src={`http://localhost:1337${item?.data?.attributes?.image?.data?.attributes?.formats?.medium?.url}`}
                         style={{objectFit: 'contain'}}/>
                </Box>

                {/*    ACTIONS*/}
                <Box flex={'1 1 50%'}
                     mb={'40px'}
                >
                    <Box
                        display={'flex'}
                        justifyContent={'space-between'}

                    >
                        <Box>Home/Item</Box>
                        <Box display={'flex'} gap={'20px'}>
                            <Box>Prev </Box>
                            <Box>Next</Box></Box>
                    </Box>
                    <Box
                        m={'65px 0 25px 0'}
                    >
                        <Typography variant={'h3'}>{item?.data?.attributes?.name}</Typography>
                        <Typography>${item?.data?.attributes?.price}</Typography>
                        <Typography sx={{
                            mt: '20px',
                        }}>{item?.data?.attributes?.longDescription}</Typography>
                    </Box>

                    <Box
                        display={'flex'}
                        alignItems={'center'}
                        minHeight={'50px'}
                    >
                        <Box
                            p='2px 5px' display={'flex'} alignItems={'center'}
                            border={`1.5px solid ${shades.neutral[300]}`} bgcolor={shades.secondary}
                            mr={'20px'}>
                            <IconButton onClick={() => setCount(Math.max(count - 1, 1))}>
                                <RemoveIcon/>
                            </IconButton>
                            <Typography
                                sx={{p: '0 5px'}}
                            >{count}</Typography>
                            <IconButton onClick={() => setCount(prev => prev + 1)}>
                                <AddIcon/>
                            </IconButton>
                        </Box>
                        <Button
                            onClick={() => dispatch(addTocart({item: {...item.data, count}}))}
                            variant={'contained'}>
                            ADD TO
                            CART</Button>
                    </Box>

                    <Box>
                        <Box
                            m={'20px 0 5px 0'} display={'flex'}
                        >
                            <FavoriteBorderOutlinedIcon/>
                            <Typography sx={{ml: '10px'}}>Add to wishlist</Typography>


                        </Box>

                        <Typography>CATEGORIES: {item?.data?.attributes.category} </Typography>
                    </Box>
                </Box>
            </Box>
            {/*Information*/}

            <Box
                m={'20px 0'}
            >
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab value="description" label="Description"/>
                    <Tab value="additional" label="Additional Information"/>
                    <Tab value="reviews" label="Reviews"/>
                </Tabs>
                {value === 'description' && <Box p={3}>{item?.data?.attributes?.longDescription}</Box>}
                {value === 'additional' && <Box p={3}>Additional Information</Box>}
                {value === 'reviews' && <Box p={3}>Reviews</Box>}
            </Box>
            <Box mt={'50px'} width={'100%'}>

                <Typography variant={'h3'} fontWeight={'bold'}>Related Items</Typography>
                <Box
                    mt={'20px'}
                    display={'flex'}
                    flexWrap={'wrap'}
                    columnGap={'1.33%'}
                    justifyContent={'space-between'}

                >
                    {items?.data?.slice(0, 4).map(item => <Item key={item.id} item={item}/>)}

                </Box>


            </Box>
        </Box>
    )
}

export default ItemDetails

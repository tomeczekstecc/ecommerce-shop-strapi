import {Box, Checkbox, FormControlLabel, Typography} from "@mui/material";
import AddressForm from "./AddressForm";

const Shipping = ({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      setFieldValue,
                  }) => {
    return (
        <Box
            m={'30px auto'}>
            <Box>
                <Typography sx={{mb: '15px'}} variant={'h4'}>
                    Billing Information
                </Typography>
                <AddressForm
                    type={'billingAddress'}
                    values={values.billingAddress}
                    errors={errors}
                    touched={touched}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                />
            </Box>

            <Box sx={{mb: '20px'}}>
                <FormControlLabel
                    label={'Use the same address for shipping'}
                    control={
                        <Checkbox
                            defaultChecked
                            value={values.shippingAddress.isSameAddress}
                        />}
                    onChange={(e) => {
                        setFieldValue('shippingAddress.isSameAddress', !values.shippingAddress.isSameAddress)
                    }}
                />

            </Box>
            {!values.shippingAddress.isSameAddress &&

                <Box>
                    <Typography sx={{mb: '15px'}} variant={'h4'}>
                        Shipping Information
                    </Typography>
                    <AddressForm
                        type={'shippingAddress'}
                        values={values.shippingAddress}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                    />
                </Box>

            }

        </Box>)

}

export default Shipping

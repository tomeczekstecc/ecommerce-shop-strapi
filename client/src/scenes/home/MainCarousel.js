import {Carousel} from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import {shades} from "../../theme";
import {Box, IconButton, Typography, useMediaQuery} from "@mui/material";

const importAll = r =>
    r.keys().reduce((images, item) => {
        images[item.replace('./', '')] = r(item);
        return images;
    }, {});

const heroTexturesImports = importAll(require.context('../../assets', false, /\.(png|jpe?g|svg)$/));

const MainCarousel = () => {
    const isNonMobile = useMediaQuery('(min-width:600px)');
    return (
        <>
            <Carousel
                infiniteLoop={true}
                showThumbs={false}
                showIndicators={true}
                showStatus={true}
                thumbWidth={200}
                dynamicHeight={true}
                autoPlay={true}
                interval={5000}
                stopOnHover={false}
                renderArrowPrev={(onClickHandler, hasPrev, label) =>
                    <IconButton onClick={onClickHandler}
                                sx={{
                                    position: 'absolute',
                                    top: '50%',
                                    left: '0',
                                    transform: 'translateY(-50%)',
                                    color: 'white',
                                    cursor: 'pointer',
                                    zIndex: 10
                                }}>
                        <NavigateBeforeIcon
                            sx={{fontSize: '40px'}}/>
                    </IconButton>
                }
                renderArrowNext={(onClickHandler, hasNext, label) =>
                    <IconButton onClick={onClickHandler}
                                sx={{
                                    position: 'absolute',
                                    top: '50%',
                                    right: '0',
                                    transform: 'translateY(-50%)',
                                    color: 'white',
                                    cursor: 'pointer',
                                    zIndex: 10
                                }}>
                        <NavigateNextIcon
                            sx={{fontSize: '40px'}}/>

                    </IconButton>
                }
            >
                {Object.keys(heroTexturesImports).map((texture, index) => {
                        return (
                            <Box key={index}>
                                <img src={heroTexturesImports[texture]} alt={texture}
                                     style={{
                                         width: '100%',
                                         height: '700px',
                                         objectFit: 'cover',
                                         backgroundAttachment: 'fixed',
                                     }}
                                />
                                <Box

                                    color='White'
                                    padding='20px'
                                    borderRight='1px'
                                    alignItems={'left'}
                                    backgroundColor={'rgba(0,0,0,0.4)'}
                                    position={'absolute'}
                                    top={'45%'}
                                    left={isNonMobile ? '10%' : 0}
                                    right={isNonMobile ? undefined : 0}
                                    margin={isNonMobile ? undefined : '0 auto'}
                                    maxWidth={isNonMobile ? undefined : '240px'}

                                >
                                    <Typography color={shades.secondary[200]}>
                                        --NEW ITEMS--

                                    </Typography>
                                    <Typography variant={'h1'} color={shades.secondary[200]}>
                                        Summer Collection
                                    </Typography>
                                    <Typography sx={{textDecoration: 'underline'}} fontWeight={'bold'}
                                                color={shades.secondary[300]}>Discover the latest
                                        trends</Typography>
                                </Box>
                            </Box>
                        )
                    }
                )}
            </Carousel>
        </>
    )
}

export default MainCarousel

import {useEffect} from 'react'
import Footer from "./scenes/global/Footer";
import {
    BrowserRouter as Router,
    Route,
    useLocation, Routes,
} from 'react-router-dom'
import Home from "./scenes/home/Home";
import ItemDetails from "./scenes/itemDetails/ItemDetails";
import Checkout from "./scenes/checkout/Checkout";
import Confirmation from "./scenes/checkout/Confirmation";
import Navbar from "./scenes/global/Navbar";
import CartMenu from "./scenes/global/CartMenu";

const ScrollToTop = () => {
    const {pathname} = useLocation()
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [pathname])
    return null
}


const App = () => {
    return (
        <Router>
            <ScrollToTop/>
            <CartMenu/>
            <Navbar/>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="item/:itemId" element={<ItemDetails/>}/>
                <Route path="checkout" element={<Checkout/>}/>
                <Route path="checkout/success" element={<Confirmation/>}/>
            </Routes>
            <Footer/>
        </Router>
    )
}

export default App

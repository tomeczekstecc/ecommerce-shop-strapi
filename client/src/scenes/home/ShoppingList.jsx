import React, {useState, useEffect} from 'react';
import {Tabs, Tab, Box, Typography, useMediaQuery} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import Item from "../../components/Item";
import {setItems} from "../../state";


const ShoppingList = () => {
    const dispatch = useDispatch();
    const [value, setValue] = useState('all');
    const items = useSelector(state => state.cart.items);
    const isNonMobile = useMediaQuery('(min-width: 600px)');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    }

    const topRatedItems = items?.filter(item => item.attributes.category === 'topRated');
    const newArrivalsItems = items?.filter(item => item.attributes.category === 'newArrivals');
    const bestSellersItems = items?.filter(item => item.attributes.category === 'bestSellers');

    async function fetchItems() {
        const items = await fetch('http://localhost:1337/api/items?populate=image',
            {
                method: 'GET',
            })
        const itemsJson = await items.json();

        dispatch(setItems(itemsJson.data));
    }

    useEffect(() => {
        fetchItems();

    }, []);

    return (
        <Box
            width={'80%'}
            margin={'80px auto'}
        >

            <Typography
                textAlign={'center'}
                variant={'h3'}>
                Our featured <b>products</b>
            </Typography>

            <Tabs
                textColor={'primary'}
                indicatorColor={'primary'}
                value={value}
                onChange={handleChange}
                centered
                TabIndicatorProps={{sx: {display: isNonMobile ? 'block' : 'none'}}}
                sx={{
                    mt: '25px',
                    "& .MuiTabs-indicator": {
                        flexWrap: 'wrap',
                    }
                }}
            >

                <Tab label={'All'} value={'all'}/>
                <Tab label={'NEW ARRIVALS'} value={'newArrivals'}/>
                <Tab label={'BEST SELLERS'} value={'bestSellers'}/>
                <Tab label={'TOP RATED'} value={'topRated'}/>

            </Tabs>
            <Box
                margin={'20px auto'}
                display={'grid'}
                // gridAutoColumns={'repeat(auto-fit, minmax(300px, 1fr))'}
                gridTemplateColumns='repeat(auto-fill, 300px)'
                justifyContent='space-around'
                rowGap={'20px'}
                columnGap={'1.33%'}
            >
                {value === 'all' && items?.map(item => <Item key={item.name + item.id.toString()} item={item}/>)}
                {value === 'newArrivals' && newArrivalsItems?.map(item => <Item key={item.name + item.id.toString()}
                                                                                item={item}/>)}
                {value === 'bestSellers' && bestSellersItems?.map(item => <Item key={item.name + item.id.toString()}
                                                                                item={item}/>)}
                {value === 'topRated' && topRatedItems?.map(item => <Item key={item.name + item.id.toString()}
                                                                          item={item}/>)}
            </Box>
        </Box>
    )
}

export default ShoppingList;

import {useState} from "react";
import {useDispatch} from "react-redux";
import {IconButton, Button, Box, Typography, useTheme} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import {shades} from "../theme";
import {addTocart} from "../state";
import {useNavigate} from "react-router-dom";

const Item = ({item, width}) => {
    console.log(item);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [count, setCount] = useState(1);
    const [isHovered, setIsHovered] = useState(false);
    const {palette: {neutral}} = useTheme();
    const {category, name, price, image} = item.attributes;
    const {id} = item;
    const {data: {attributes: {formats: {medium: {url}}}}} = image;
    return (
        <Box
            width={width}
            position={'relative'}
            onMouseOver={() => setIsHovered(true)}
            onMouseOut={() => setIsHovered(false)}
        >
            <img
                onClick={() => navigate(`/item/${id}`)}
                alt={item?.name}
                width='300px'
                height='400px'
                src={url}
                style={{cursor: 'pointer'}}
            />
            <Box
                display={isHovered ? 'block' : 'none'}
                position={'absolute'}
                left={0}
                bottom={'20%'}
                width={'100%'}
                padding={'0 5%'}
            >

                <Box
                    display={'flex'}
                    justifyContent={'space-between'}
                >
                    <Box
                        alignItems={'center'}
                        backgroundColor={shades.neutral[100]}
                        borderRadius={'3px'}
                        display={'flex'}>
                        <IconButton onClick={() => setCount(Math.max(count - 1, 1))}>
                            <RemoveIcon/>
                        </IconButton>
                        <Typography color={shades.primary["300"]}>{count}</Typography>
                        <IconButton onClick={() => setCount(count + 1)}>
                            <AddIcon/>
                        </IconButton>
                    </Box>
                    <Button
                        sx={{
                            backgroundColor: shades.primary[300],
                            color: 'white',
                            "&:hover": {
                                backgroundColor: shades.primary[200],
                            }
                        }}
                        onClick={() => dispatch(addTocart({item: {...item, count}}))}
                    >
                        ADD TO CART
                    </Button>
                </Box>
            </Box>
            <Box mt={'3px'}>
                <Typography variant={'subtitle2'} color={shades.primary[500]} fontWeight={'bold'}>
                    {category.replace(/([A-Z])/g, ' $1')
                        .replace(/^./, str => str.toUpperCase())}
                </Typography>
                <Typography>{name}</Typography>
                <Typography color={shades.primary[300]} fontWeight={'bold'}>${price}</Typography>
            </Box>
        </Box>
    )


}
export default Item;

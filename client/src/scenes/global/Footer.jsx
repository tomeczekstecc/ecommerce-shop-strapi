import {useTheme} from "@mui/material";
import {Box, Typography} from "@mui/material";
import {shades} from "../../theme";


const Footer = () => {
    const {palette: {neutral}} = useTheme();
    return (
        <footer>
            <Box p={'40px 0'}
                 backgroundColor={neutral.light}
            >
                <Box
                    width={'80%'}
                    margin={'auto'}
                    display={'flex'}
                    justifyContent={'space-between'}
                    flexWrap={'wrap'}
                    rowGap={'30px'}
                    columnGap={'clamp(20px, 30px, 40px)'}
                >
                    <Box
                        width={'clamp(20px, 30%, 40%)'}

                    >
                        <Typography variant={'h4'} fontWeight={'bold'} mb={'30px'}
                                    color={shades.secondary[500]}>ECOMMER</Typography>
                        <div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur at illum
                            maiores natus sint veniam? Cupiditate deleniti deserunt ea labore! Ad adipisci, architecto
                            at beatae culpa deserunt earum fuga in iste iure minima neque non provident quam quos rem?
                        </div>
                    </Box>
                    <Box>
                        <Typography variant={'h6'} fontWeight={'bold'} mb={'30px'}>About us</Typography>
                        <Typography mb={'30px'}>Careers</Typography>
                        <Typography mb={'30px'}>Our Stores</Typography>
                        <Typography mb={'30px'}>Privacy</Typography>
                        <Typography mb={'30px'}>Policy</Typography>
                    </Box>
                    <Box>
                        <Typography variant={'h6'} fontWeight={'bold'} mb={'30px'}>Customer Care</Typography>
                        <Typography mb={'30px'}>Help center</Typography>
                        <Typography mb={'30px'}>Track Order</Typography>
                        <Typography mb={'30px'}>Corporate</Typography>
                        <Typography mb={'30px'}>Return</Typography>
                    </Box>
                    <Box
                        width={'clamp(20%, 25%, 30%)'}
                    >
                        <Typography variant={'h6'} fontWeight={'bold'} mb={'30px'}>Contact Us</Typography>
                        <Typography mb={'30px'}>50 North Blvd. Washington DC</Typography>
                        <Typography mb={'30px'}>Email: lorem@email.com</Typography>
                        <Typography mb={'30px'}>(222) 333 4444 </Typography>

                    </Box>

                </Box>


            </Box>
        </footer>
    );
}


export default Footer;
